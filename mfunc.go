package mfunc

import (
	"sync"
)

type (
	mapfunc    func(data []interface{}, index int) interface{}
	filterfunc func(data []interface{}, index int) bool
	reducefunc func(d1 interface{}, d2 interface{}) interface{}
)

func Maps(data []interface{}, f mapfunc) []interface{} {
	r := make([]interface{}, len(data))
	for i := 0; i < len(data); i++ {
		r[i] = f(data, i)
	}
	return r
}

func Mapx(data []interface{}, f mapfunc) []interface{} {
	r := make([]interface{}, len(data))
	var w sync.WaitGroup
	for i := 0; i < len(data); i++ {
		w.Add(1)
		go func(ix int) {
			r[ix] = f(data, ix)
			w.Done()
		}(i)
	}
	w.Wait()
	return r
}

func Filters(data []interface{}, f filterfunc) []interface{} {
	var r []interface{}
	for i := 0; i < len(data); i++ {
		if f(data, i) {
			r = append(r, data[i])
		}
	}
	return r
}

func Filterx(data []interface{}, f filterfunc) []interface{} {
	var r []interface{}
	var w sync.WaitGroup
	var mutex = &sync.Mutex{}
	for i := 0; i < len(data); i++ {
		w.Add(1)
		go func(ix int) {
			if f(data, ix) {
				mutex.Lock()
				r = append(r, data[ix])
				mutex.Unlock()
			}
			w.Done()
		}(i)
	}
	w.Wait()
	return r
}

func Reduces(data []interface{}, f reducefunc) interface{} {
	if len(data) < 2 {
		return data[0]
	}
	r := data[0]
	for i := 1; i < len(data); i++ {
		r = f(r, data[i])
	}
	return r
}

func Reducex(data []interface{}, f reducefunc) interface{} {
	if len(data) < 2 {
		return data[0]
	}
	if len(data) < 3 {
		return f(data[0], data[1])
	}
	if len(data) < 4 {
		return f(Reducex(data[0:2], f), data[2])
	}

	mid := len(data) / 2
	ch := make(chan interface{})
	go func() {
		ch <- Reducex(data[0:mid], f)
	}()
	go func() {
		ch <- Reducex(data[mid:len(data)], f)
	}()
	return f(<-ch, <-ch)
}
