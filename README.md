# mfunc
Map, Filter and Reduce implement in golang with interface{}

*This project is developed for my personal used. You may use it on your own risk or modify it as you wish.*

**Example**

```go
package main 

import (
  "fmt"
  "gitlab.com/authapon/mfunc"
)

func main() {
  data := make([]interface{}, 40)
  for i := 0; i < 40; i++ {
    data[i] = i + 1
  }

  rdata := mfunc.Mapx(data, func(datax []interface{}, index int) interface{} {
    dd := fib(datax[index].(int))
    fmt.Printf("%d -> %d\n", datax[index], dd)
    return dd
  })

  fmt.Printf("\nSTOP\n")
  for i := range rdata {
    fmt.Printf("%d\n", rdata[i].(int))
  }
  
  r2data := mfunc.Filterx(rdata, func(datax []interface{}, index int) bool {
    if (datax[index].(int) % 2) == 0 {
      return true
    }
    return false
  })

  fmt.Printf("\nFilter\n")
  for i := range r2data {
    fmt.Printf("%d\n", r2data[i].(int))
  }

  fmt.Printf("\nReduce\n")
  rr := mfunc.Reducex(r2data, func(d1 interface{}, d2 interface{}) interface{} {
    return d1.(int) + d2.(int)
  })
  fmt.Printf("%d\n", rr)
}
 
func fib(n int) int {
  if n < 2 {
    return n
  }
  return fib(n-1) + fib(n-2)
}

```
